// ==UserScript==
// @name         24SevenOffice products section Chrome polyfills
// @namespace    https://app.24sevenoffice.com/script/Produkter/
// @version      0.1
// @description  Nop
// @author       MY
// @run-at       document-start
// @match        https://app.24sevenoffice.com/script/Produkter/produkter/Product_main.asp*
// @match        https://app.24sevenoffice.com/script/Produkter/produkter/product_list.asp*
// @match        https://app.24sevenoffice.com/script/Produkter/categories/menu.asp*
// @grant        none
// ==/UserScript==

if (!window.createPopup) {
  window.createPopup = function () {
    var popup = document.createElement("iframe"), //must be iframe because existing functions are being called like parent.func()
      isShown = false,
      popupClicked = false;
    popup.src = "about:blank";
    popup.style.position = "absolute";
    popup.style.border = "0px";
    popup.style.display = "none";
    popup.addEventListener("load", function (e) {
      popup.document = (popup.contentWindow || popup.contentDocument); //this will allow us to set innerHTML in the old fashion.
      if (popup.document.document) popup.document = popup.document.document;
    });
    document.body.appendChild(popup);
    var hidepopup = function (event) {
      if (isShown) {
        setTimeout(function () {
          if (!popupClicked) {
            popup.hide();
          }
          popupClicked = false;
        }, 150); //timeout will allow the click event to trigger inside the frame before closing.
      }
    };

    popup.show = function (x, y, w, h, pElement) {
      if (typeof (x) !== 'undefined') {
        var elPos = [0, 0];
        if (pElement) elPos = findPos(pElement); //maybe validate that this is a DOM node instead of just falsy
        elPos[0] += y;
        elPos[1] += x;

        if (isNaN(w)) w = popup.document.scrollWidth;
        if (isNaN(h)) h = popup.document.scrollHeight;
        if (elPos[0] + w > document.body.clientWidth) elPos[0] = document.body.clientWidth - w - 5;
        if (elPos[1] + h > document.body.clientHeight) elPos[1] = document.body.clientHeight - h - 5;

        popup.style.left = elPos[0] + "px";
        popup.style.top = elPos[1] + "px";
        popup.style.width = w + "px";
        popup.style.height = h + "px";
      }
      popup.style.display = "block";
      isShown = true;
    };

    popup.hide = function () {
      isShown = false;
      popup.style.display = "none";
    };

    window.addEventListener('click', hidepopup, true);
    window.addEventListener('blur', hidepopup, true);
    return popup;
  }
}

function findPos(obj, foundScrollLeft, foundScrollTop) {
  var curleft = 0;
  var curtop = 0;
  if (obj.offsetLeft) curleft += parseInt(obj.offsetLeft);
  if (obj.offsetTop) curtop += parseInt(obj.offsetTop);
  if (obj.scrollTop && obj.scrollTop > 0) {
    curtop -= parseInt(obj.scrollTop);
    foundScrollTop = true;
  }
  if (obj.scrollLeft && obj.scrollLeft > 0) {
    curleft -= parseInt(obj.scrollLeft);
    foundScrollLeft = true;
  }
  var pos = [0, 0];
  if (obj.offsetParent) {
    pos = findPos(obj.offsetParent, foundScrollLeft, foundScrollTop);
    curleft += pos[0];
    curtop += pos[1];
  } else if (obj.ownerDocument) {
    var thewindow = obj.ownerDocument.defaultView;
    if (!thewindow && obj.ownerDocument.parentWindow) {
      thewindow = obj.ownerDocument.parentWindow;
    }
    if (thewindow) {
      if (!foundScrollTop && thewindow.scrollY && thewindow.scrollY > 0) curtop -= parseInt(thewindow.scrollY);
      if (!foundScrollLeft && thewindow.scrollX && thewindow.scrollX > 0) curleft -= parseInt(thewindow.scrollX);
      if (thewindow.frameElement) {
        pos = findPos(thewindow.frameElement);
        curleft += pos[0];
        curtop += pos[1];
      }
    }
  }
  return [curleft, curtop];
}

if (EventTarget.prototype.attachEvent == null) {
  Object.defineProperty(EventTarget.prototype, 'attachEvent', {
    value: function (event, func) {
      if ('string' !== typeof event || event.indexOf('on') !== 0) {
        return;
      }
      this.addEventListener(event.substring(2), func, false);
    },
    enumerable: false
  });
}

if (EventTarget.prototype.detachEvent == null) {
  Object.defineProperty(EventTarget.prototype, 'detachEvent', {
    value: function (event, func) {
      if ('string' !== typeof event || event.indexOf('on') !== 0) {
        return;
      }
      this.removeEventListener(event.substring(2), func, false);
    },
    enumerable: false
  });
}
if (!("currentStyle" in Element.prototype)) {
  Object.defineProperty(Element.prototype, 'currentStyle', {
    get: function () {
      return this.constructor ? window.getComputedStyle(this) : null;
    }
  });
}


if (window.location.href.indexOf("https://app.24sevenoffice.com/script/Produkter/categories/menu.asp") > -1) {
  setInterval(function () {
    var i, frames;
    frames = document.getElementsByTagName("iframe");
    for (i = 0; i < frames.length; ++i) {
      if (frames[i].contentWindow.EventTarget.prototype.attachEvent == null) {
        Object.defineProperty(frames[i].contentWindow.EventTarget.prototype, 'attachEvent', {
          value: function (event, func) {
            if ('string' !== typeof event || event.indexOf('on') !== 0) {
              return;
            }
            this.addEventListener(event.substring(2), func, false);
          },
          enumerable: false
        });
      }
      if (frames[i].contentWindow.EventTarget.prototype.detachEvent == null) {
        Object.defineProperty(frames[i].contentWindow.EventTarget.prototype, 'detachEvent', {
          value: function (event, func) {
            if ('string' !== typeof event || event.indexOf('on') !== 0) {
              return;
            }
            this.removeEventListener(event.substring(2), func, false);
          },
          enumerable: false
        });
      }
      if (!("currentStyle" in frames[i].contentWindow.Element.prototype)) {
        Object.defineProperty(frames[i].contentWindow.Element.prototype, 'currentStyle', {
          get: function () {
            return this.constructor ? window.getComputedStyle(this) : null;
          }
        });
      }
      if (!("parentWindow" in frames[i].contentWindow.Document.prototype)) {
        Object.defineProperty(frames[i].contentWindow.Document.prototype, 'parentWindow', {
          get: function () {
            return document.defaultView;
          }
        });
      }
    }
  }, 100);

  window.attachEvent('onload', function () {
    // Trigger errors
    setTimeout(function () {
      if (typeof cMenu !== 'undefined') {
        cMenu.show(0, 0);
      }
    }, 10);
    setTimeout(function () {
      if (typeof cMenu !== 'undefined') {
        cMenu.show(0, 0);
      }
    }, 150);
    setTimeout(function () {
      if (typeof cMenu !== 'undefined') {
        cMenu.close();
      }
    }, 250);
  });
}

console.log('Pofyfills for Chrome are injected!');